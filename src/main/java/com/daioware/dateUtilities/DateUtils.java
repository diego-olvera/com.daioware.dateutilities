package com.daioware.dateUtilities;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class DateUtils {

	public static boolean isExpiredDate(Instant dateToEvaluate,long maxMinutes) {
		//or
		//return ChronoUnit.MINUTES.between(dateToEvaluate, Instant.now())>maxMinutes;
		return dateToEvaluate.until(Instant.now(),ChronoUnit.MINUTES)>maxMinutes;
	}
	public static boolean isExpiredDate(LocalDateTime dateToEvaluate,long maxMinutes) {
		return dateToEvaluate.until(LocalDateTime.now(),ChronoUnit.MINUTES)>maxMinutes;
	}
	public static int age(LocalDate birthday, LocalDateTime date) {
		return birthday.until(date.toLocalDate()).getYears();
	}
	public static int age(LocalDate birthday) {
		return age(birthday,LocalDateTime.now());
	}
}
