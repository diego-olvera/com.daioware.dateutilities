package com.daioware.dateUtilities;

import java.time.Instant;

public class TimeInterval {
	private Instant startDate;
	private Instant finishDate;
	
	public TimeInterval() {
	}

	public TimeInterval(Instant startDate, Instant finishDate) {
		setStartDate(startDate);
		setFinishDate(finishDate);
	}

	public Instant getStartDate() {
		return startDate;
	}

	public Instant getFinishDate() {
		return finishDate;
	}

	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	public void setFinishDate(Instant finishDate) {
		this.finishDate = finishDate;
	}

	
	
}
