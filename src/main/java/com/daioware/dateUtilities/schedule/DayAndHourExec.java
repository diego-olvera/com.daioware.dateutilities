package com.daioware.dateUtilities.schedule;

import java.time.DayOfWeek;
import java.util.HashMap;

public class DayAndHourExec {
	private DayOfWeek day;
	private HashMap<Integer,ExecutionHour> hours=new HashMap<>();
	private boolean finished;
	public DayAndHourExec(DayOfWeek day) {
		setDay(day);
		hours=new HashMap<>();
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	public boolean finishedAllHours() {
		for(ExecutionHour h:hours.values()) {
			if(!h.isExecuted()) {
				return false;
			}
		}
		return true;
	}
	public void setHours(HashMap<Integer, ExecutionHour> hours) {
		this.hours = hours;
	}

	public DayOfWeek getDay() {
		return day;
	}

	public void setDay(DayOfWeek day) {
		this.day = day;
	}
	public ExecutionHour addHour(ExecutionHour hour) {
		return hours.put(hour.getHour(),hour);
	}
	public ExecutionHour removeHour(int hour) {
		return hours.remove(hour);
	}
	public ExecutionHour getHour(int hour) {
		return hours.get(hour);
	}
	public void addHours(int firstHour,int endHour,int min) {
		for(int hour=firstHour;hour<=endHour;hour++) {
			addHour(new ExecutionHour(hour, min));
		}
	}
	public void addHours(int firstHour,int endHour) {
		addHours(firstHour,endHour,0);
	}
	public void addHours(int firstHour) {
		addHours(firstHour,23,0);
	}
	public void addHours() {
		addHours(0,23);
	}

	public HashMap<Integer, ExecutionHour> getHours() {
		return hours;
	}
}
