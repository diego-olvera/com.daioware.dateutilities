package com.daioware.dateUtilities.schedule;

import java.time.DayOfWeek;
import java.util.HashMap;

public class DayHour {
	private DayOfWeek day;
	private HashMap<Integer,Hour> hours=new HashMap<>();
	private boolean finished;
	public DayHour(DayOfWeek day) {
		setDay(day);
		hours=new HashMap<>();
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	public void setHours(HashMap<Integer,Hour> hours) {
		this.hours = hours;
	}

	public DayOfWeek getDay() {
		return day;
	}

	public void setDay(DayOfWeek day) {
		this.day = day;
	}
	public Hour addHour(Hour hour) {
		return hours.put(hour.getHour(),hour);
	}
	public void addHours(int firstHour,int endHour,int min) {
		for(int hour=firstHour;hour<=endHour;hour++) {
			addHour(new Hour(hour, min));
		}
	}
	public void addHours(int firstHour,int endHour) {
		addHours(firstHour,endHour,0);
	}
	public void addHours(int firstHour) {
		addHours(firstHour,23,0);
	}
	public void addHours() {
		addHours(0,23);
	}

	public HashMap<Integer, Hour> getHours() {
		return hours;
	}

	
}
