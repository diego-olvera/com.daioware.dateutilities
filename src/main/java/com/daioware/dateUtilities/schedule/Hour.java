package com.daioware.dateUtilities.schedule;

public class Hour {
	private int hour;
	private int minute;
	
	public Hour(int hour) {
		this(hour,0);
	}
	public Hour(int hour, int minute) {
		setHour(hour);
		setMinute(minute);
	}

	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public int getMinute() {
		return minute;
	}
	public void setMinute(int minute) {
		this.minute = minute;
	}
	
	public double getTotalMinutesOfDay() {
		return getHour()*60+getMinute();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getHour();
		result = prime * result + getMinute();
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Hour))
			return false;
		Hour other = (Hour) obj;
		if (getHour() != other.getHour())
			return false;
		if (getMinute() != other.getMinute())
			return false;
		return true;
	}
	
}
