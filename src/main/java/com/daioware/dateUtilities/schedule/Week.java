package com.daioware.dateUtilities.schedule;

import java.time.DayOfWeek;
import java.util.HashMap;

public class Week {
	
	private HashMap<DayOfWeek,DayAndHourExec> days; 
	private int number;
	
	public Week() {
		days=new HashMap<>();
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	public DayAndHourExec addDay(DayAndHourExec day) {
		return days.put(day.getDay(),day);
	}
	public DayAndHourExec removeDay(DayOfWeek d) {
		return days.remove(d);
	}
	public DayAndHourExec getDay(DayOfWeek d) {
		return days.get(d);
	}

	public HashMap<DayOfWeek, DayAndHourExec> getDays() {
		return days;
	}
	
}
