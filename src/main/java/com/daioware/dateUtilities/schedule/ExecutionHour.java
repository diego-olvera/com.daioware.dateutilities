package com.daioware.dateUtilities.schedule;

public class ExecutionHour extends Hour{

	private boolean executed;
	public ExecutionHour(int hour) {
		this(hour,0);
	}
	public ExecutionHour(int hour, int minute) {
		super(hour,minute);
	}
	public boolean isExecuted() {
		return executed;
	}
	
	public void setExecuted(boolean b) {
		executed=b;
	}
}
