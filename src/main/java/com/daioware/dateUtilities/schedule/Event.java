package com.daioware.dateUtilities.schedule;

import java.time.LocalDateTime;

public class Event {
	private String title;
	private String where;
	private String description;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
		
	public Event(String title, String where, String description, LocalDateTime startDate, LocalDateTime endDate) {
		this.title = title;
		this.where = where;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getWhere() {
		return where;
	}
	public void setWhere(String where) {
		this.where = where;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getStartDate() {
		return startDate;
	}
	public boolean setStartDate(LocalDateTime e) {
		if(e!=null) {
			startDate=e;
			return true;
		}
		else {
			return false;
		}
	}
	public LocalDateTime getEndDate() {
		return endDate;
	}
	public boolean setEndDate(LocalDateTime e) {
		if(e!=null && e.isAfter(getStartDate())) {
			endDate=e;
			return true;
		}
		else {
			return false;
		}
	}	
}
