package com.daioware.dateUtilities;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.TimeZone;

public class CurrentDate{
	
	private static ZonedDateTime getCurrentZonedDateTime(){
		return getCurrentZonedDateTime(TimeZone.getDefault().toZoneId());
	}
	private static ZonedDateTime getCurrentZonedDateTime(ZoneId zoneId){
		return LocalDateTime.now().atZone(zoneId);
	}
	public static DayOfWeek getWeekDay(ZoneId zoneId) {
        return getCurrentZonedDateTime(zoneId).getDayOfWeek();
	}
	public static DayOfWeek getWeekDay() {
        return getCurrentZonedDateTime().getDayOfWeek();
	}
    public static int getDay(){
        return getCurrentZonedDateTime().getDayOfMonth(); 
    }
    public static int getDay(ZoneId zoneId){
        return getCurrentZonedDateTime(zoneId).getDayOfMonth(); 
    }
    public  static Month getMonth(){
        return getCurrentZonedDateTime().getMonth();
    }
    public  static Month getMonth(ZoneId zoneId){
        return getCurrentZonedDateTime(zoneId).getMonth();
    }
    public  static int getYear(){
        return getCurrentZonedDateTime().getYear();
    }
    public  static int getYear(ZoneId zoneId){
        return getCurrentZonedDateTime(zoneId).getYear();
    }
    public static int getDayOfYear() {
        return getCurrentZonedDateTime().getDayOfYear();
    }
    public static int getDayOfYear(ZoneId zoneId) {
        return getCurrentZonedDateTime(zoneId).getDayOfYear();
    }
    public static int getHour(){
    	return getCurrentZonedDateTime().getHour();
    }
    public static int getHour(ZoneId zoneId){
    	return getCurrentZonedDateTime(zoneId).getHour();
    }
    public static int getMinutes(){
    	return getCurrentZonedDateTime().getMinute();
    }
    public static int getMinutes(ZoneId zoneId){
    	return getCurrentZonedDateTime(zoneId).getMinute();
    }
    public static int getMinutesOfDay(){
    	return getHour()*60+getMinutes();
    }
    public static int getMinutesOfDay(ZoneId zoneId){
    	return getHour(zoneId)*60+getMinutes(zoneId);
    }
    public static int getSecondsOfDay(){
    	return getMinutesOfDay()/60;
    }
    public static int getSecondsOfDay(ZoneId zoneId){
    	return getMinutesOfDay(zoneId)/60;
    }
    public static int getSeconds(){
    	return getCurrentZonedDateTime().getSecond();
    }
    public static int getSeconds(ZoneId zoneId){
    	return getCurrentZonedDateTime(zoneId).getSecond();
    }
}
